## 2019 iHEA World Congress: New Heights in Health Economics
### Date
14.07.2019 to 17.07.2019

### Place
Basel (Switzerland)

### Title
"_Observed over expected nurse staffing ratio used as an estimator for individual patient variation over length of stay_" 