## UseR! 2019
### Date
09.07.2019 to 12.07.2019

### Place
Toulouse (France)

### Title
"_Variation of patient turnover on a 30-minutes basis for 3 years: analysis of routine data of a Swiss University Hospital_"

### Youtube link to the talk
[Youtube](https://www.youtube.com/watch?v=uax2vMU5a4k)